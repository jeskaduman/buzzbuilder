# Buzzbuilder

A full-screen, responsive one-page website with a newsletter signup box. Designed to generate advance buzz for a brand or event by collecting email addresses for upcoming announcements.

Version one features:
- [Milligram](https://milligram.io/) for basic grids & styling
- Responsive full-screen background with [Backstretch (jquery)](https://www.jquery-backstretch.com/)
- Fully responsive - two column layout converts to single column on small and medium screen sizes
